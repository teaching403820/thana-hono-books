import {Context} from 'hono';
import {invalidBodyException, invalidValidationResponse} from '../exceptions/validation-exception';
import {paginationValidator} from '../validators/pagination-validators';


/**
 * Parse the request body and return the data as json,
 * if or when parse fails, returns a http-exception.
 */
export const parseBody = async <T>(context: Context) => {
   let rawBody: T;
   
   try {
      rawBody = await context.req.json<T>();
   } catch (e) {
      throw invalidBodyException();
   }
   return rawBody;
};


export const parsePaginationQuery = async (c: Context) => {
      const rawQuery = c.req.query();
      return await paginationValidator.validate(rawQuery);
};