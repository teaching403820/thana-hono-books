import bcrypt from 'bcrypt';

const SALT_ROUNDS = 12;


/**
 * Hash the password using b-crypt
 */
export const hashPassword = async (password: string) => {
   return await bcrypt.hash(password, SALT_ROUNDS);
};

/**
 * Compares the plain-text password with the hash provided
 */
export const comparePassword = async (password: string, hash: string) => {
   return await bcrypt.compare(password, hash);
};


