export interface Pagination {
   firstPage: number,
   currentPage: number,
   lastPage: number,
   perPage: number
   total: number
}


/**
 *
 */
export const getPaginationData = (query: { limit: number, page: number }, total: number) => {
   
   let calculatedOffset = (query.page - 1) * query.limit
   
   let lastPage = Math.ceil(total / query.limit)
   
   const data: Pagination = {
      currentPage: query.page,
      firstPage: 1,
      lastPage: lastPage,
      perPage: query.limit,
      total: total
   };
   
   
   return {
      data,
      calculatedOffset,
   };
};