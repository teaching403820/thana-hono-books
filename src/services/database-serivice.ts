import postgres from 'postgres'
import {drizzle} from 'drizzle-orm/postgres-js';


const {DB_HOST, DB_NAME, DB_USER, DB_PASS} = process.env


const client = postgres(`postgres://${DB_USER}:${DB_PASS}@${DB_HOST}:5432/${DB_NAME}`);

const db = drizzle(client);

export default db;