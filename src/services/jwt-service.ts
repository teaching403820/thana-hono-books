import {User} from '../_drizzle/schema/users-schema';
import {sign} from 'hono/jwt';


const {JWT_SECRET} = process.env;

/**
 * Sign and create a new jwt
 */
export const generateToken = async (user: User) => {
   
   const validTimeTill = 10000;
   
   const payload = {
      sub: user.email,
      role: user.role,
      name: user.fullName,
      exp: Math.floor(Date.now() / 1000) + 60 * validTimeTill
   };
   
   return await sign(payload, JWT_SECRET!);
   
};