import 'dotenv/config';
import {Hono} from 'hono';

import {serve} from '@hono/node-server';
import usersRouter from './routes/users-routes';
import booksRouter from './routes/books-routes';
import authRouter from './routes/auth-routes';
import {jwt} from 'hono/jwt';

/* ---------------------------------------------------------------------------------------------- */


const {JWT_SECRET} = process.env;


const app = new Hono();

app.get('/', (c) => {
   return c.text('Hello Hono!');
});

app.use('/users/*', jwt({secret: JWT_SECRET!}));
app.use('/books/*', jwt({secret: JWT_SECRET!}));

app.route('/', authRouter);
app.route('/users', usersRouter);
app.route('/books', booksRouter);


const port = 3000;
console.log(`Server is running on port ${port}`);

serve({
   fetch: app.fetch,
   port
});


