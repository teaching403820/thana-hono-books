import {HTTPException} from 'hono/http-exception';
import {Context} from 'hono';
import {errors} from '@vinejs/vine';


export const invalidBodyException = (message: string = 'Invalid body.') => {
   return new HTTPException(422, {message});
};


export const invalidValidationResponse = (context: Context, error: any) => {
   if (error instanceof errors.E_VALIDATION_ERROR) {
      context.status(422);
      
      return context.json(
         error.messages
      );
      
   }
};