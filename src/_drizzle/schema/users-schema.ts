import {pgTable, serial, text, timestamp, uniqueIndex} from 'drizzle-orm/pg-core';


export const UserSchema = pgTable('users', {
   id: serial('id').primaryKey(),
   email: text('email').notNull().unique(),
   fullName: text('fullName'),
   password: text('password').notNull(),
   role: text('role').$type<'ADMIN' | 'CUSTOMER'>().default('CUSTOMER'),
   createdAt: timestamp('createdAt').defaultNow(),
   updatedAt: timestamp('updatedAt').defaultNow(),
}, (table) =>{
   return {
      emailIdx: uniqueIndex('email_unique_idx').on(table.email)
   }
});

export type CreateUser = typeof UserSchema.$inferInsert
export type User = typeof UserSchema.$inferSelect