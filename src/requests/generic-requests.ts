import {Context} from 'hono';
import {StatusCode} from 'hono/utils/http-status';


/* ---------------------------------------------------------------------------------------------- */


export interface ResponsePayload {
   message: any;
}


/* ---------------------------------------------------------------------------------------------- */

const genericResponse = (context: Context, statusCode: StatusCode, payload: any) => {
   context.status(statusCode);
   return context.json(payload);
};


export const okResponse = (c: Context, payload: any) => {
   return c.json(payload);
};

export const badRequestResponse = (c: Context, payload: ResponsePayload) => {
   return genericResponse(c, 400, payload);
};

export const unauthorizedResponse = (c: Context, payload: ResponsePayload) => {
   return genericResponse(c, 401, payload);
};

export const notFoundResponse = (c: Context, payload: ResponsePayload) => {
   return genericResponse(c, 404, payload);
};