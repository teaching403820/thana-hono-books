import {Hono} from 'hono';
import {
   UserResource
} from '../resources/user-resource';
import {paginationValidator} from '../validators/pagination-validators';
import {invalidValidationResponse} from '../exceptions/validation-exception';
import {parsePaginationQuery} from '../utils/request-utils';
import {getPaginationData} from '../utils/pagination-utils';


const usersRouter = new Hono();

/**
 * Get all users
 */
usersRouter.get('/', async (c) => {
   
   
   try {
      const paginationQuery = await parsePaginationQuery(c);
      
      const total = await UserResource.getCount();
      
      const {data, calculatedOffset} = getPaginationData(paginationQuery, total);
      
      const users = await UserResource.all(paginationQuery.limit, calculatedOffset);
      
      return c.json({
         data: users,
         ...data
      });
      
   } catch (e) {
      return invalidValidationResponse(c, e);
   }
   
   
});


export default usersRouter;