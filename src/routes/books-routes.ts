import {Hono} from 'hono';

/* ---------------------------------------------------------------------------------------------- */


const booksRouter = new Hono();


booksRouter.get('/', async (c) => {
   
   return c.json({'message': 'Books'});
   
});

export default booksRouter;