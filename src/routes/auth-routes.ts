import {Hono} from 'hono';
import {comparePassword, hashPassword} from '../utils/password-utils';
import {generateToken} from '../services/jwt-service';
import {invalidValidationResponse} from '../exceptions/validation-exception';
import {loginUserValidator, registerUserValidator} from '../validators/users-validators';
import {parseBody} from '../utils/request-utils';
import {badRequestResponse, okResponse, unauthorizedResponse} from '../requests/generic-requests';
import {UserResource} from '../resources/user-resource';

/* ---------------------------------------------------------------------------------------------- */


const authRouter = new Hono();


/**
 * Register a new user
 */
authRouter.post('/register', async (c) => {
   
   
   let data = null;
   
   try {
      const rawBody = await parseBody(c);
      data = await registerUserValidator.validate(rawBody);
   } catch (e) {
      return invalidValidationResponse(c, e);
   }
   
   
   try {
      
      const existingUser = await UserResource.findByEmail(data.email);
      if (existingUser) return badRequestResponse(c, {message: 'Email already in use. Please use another email.'});
      
      
      data.password = await hashPassword(data.password);
      
      const createdUser = await UserResource.create(data);
      return okResponse(c, createdUser);
      
   } catch (e) {
      return badRequestResponse(c, {message: 'Failed to register the user.'});
   }
   
});

/**
 * Login
 */
authRouter.post('/login', async (c) => {
   
   let data = null;
   
   try {
      const rawBody = await parseBody(c);
      data = await loginUserValidator.validate(rawBody);
   } catch (e) {
      return invalidValidationResponse(c, e);
   }
   
   const user = await UserResource.findByEmail(data.email);
   
   
   if (!user) {
      return unauthorizedResponse(c, {message: 'Authentication failed.'});
   }
   
   
   if (await comparePassword(data.password, user.password)) {
      
      const token = await generateToken(user);
      
      return okResponse(c, {
         user: {
            id: user.id,
            fullName: user.fullName,
            email: user.email
         },
         token
      });
      
   }
   
   return unauthorizedResponse(c, {message: 'Authentication failed.'});
});


export default authRouter;