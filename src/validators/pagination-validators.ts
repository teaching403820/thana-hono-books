import vine from '@vinejs/vine';


export const paginationValidator = vine.compile(
   vine.object({
      page: vine.number().positive().min(1),
      limit: vine.number().positive().min(1),
   })
);