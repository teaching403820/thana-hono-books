import vine from '@vinejs/vine';
import {Infer} from '@vinejs/vine/types';


export const registerUserValidator = vine.compile(
   vine.object({
      email: vine.string().email(),
      fullName: vine.string(),
      password: vine.string().minLength(4).confirmed(),
   })
);

export type RegisterUserFields = Infer<typeof registerUserValidator>


export const loginUserValidator = vine.compile(
   vine.object({
      email: vine.string().email(),
      password: vine.string().minLength(4)
   })
);

export type LoginUserFields = Infer<typeof loginUserValidator>