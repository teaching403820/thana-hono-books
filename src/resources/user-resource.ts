import db from '../services/database-serivice';
import {CreateUser, User, UserSchema} from '../_drizzle/schema/users-schema';
import {count, eq} from 'drizzle-orm';
import Users from '../routes/users-routes';
import {undefined} from 'zod';


export class UserResource {
   
   static async getCount() {
      const response = await db.select({count: count()}).from(UserSchema);
      if (response.length > 0) return response[0].count;
      return 0;
   }
   
   
   static async find(id: number) {
      const response: User[] = await db.select().from(UserSchema).where(eq(UserSchema.id, id));
      if (response.length > 0) return response[0];
      return null;
   }
   
   
   static async findByEmail(email: string){
      const response: User[] = await db.select().from(UserSchema).where(eq(UserSchema.email, email)).limit(1);
      if (response.length > 0) return response[0];
      return null;
   }
   
   
   static async all(limit: number = 2, offset: number = 0) {
      return db.select({
         id: UserSchema.id,
         email: UserSchema.email,
         fullName: UserSchema.fullName,
         role: UserSchema.role,
      }).from(UserSchema).limit(limit).offset(offset);
   }
   
   
   static async create(user: CreateUser){
      return db.insert(UserSchema).values(user).returning({
         insertedId: UserSchema.id
      });
   }
   
}