import {defineConfig} from 'drizzle-kit';


export default defineConfig({
   dialect: 'postgresql',
   schema: './src/_drizzle/schema/*',
   dbCredentials: {
      database: 'books_db',
      user: 'postgres',
      password: 'password',
      host: 'localhost'
   }
});